cvt -r 1536 864 60
# Modeline "1536x864R"   90.25  1536 1584 1616 1696  864 867 872 889 +hsync -vsync
xrandr --newmode 1536x864 90.25  1536 1584 1616 1696  864 867 872 889 +hsync -vsync
xrandr --addmode eDP1 1536x864
xrandr --output eDP1 --mode 1536x864
