# Debian scripts


## Display resolution


1920x1080 on 13" display makes elements too small. So I picked lower 16:9 resolution based on https://pacoup.com/2011/06/12/list-of-true-169-resolutions/.

To change resolution to 1536x864


    git clone https://gitlab.com/dzaporozhets/debian-scripts.git
    sh debian-scripts/display.sh
